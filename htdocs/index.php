<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CS3140 project A03 for Jayden Egri-Blogna</title>
  <link rel="stylesheet" href="styles.css">
</head>

<div id="top">
  <h1>Assignment 3 – Egri-Blogna, Jayden</h1>
</div>

<header>
  <a href="#Grocery">Grocery List</a>
  <a href="#State">State List</a>
    <a href="#Movie">Movie List</a>
  <a href="#Breed">Breed Stats</a>
  <a href="#StateInfo">State Info</a>
  <a href="#Meals">Meals</a>
</header>

<body>

<div id="Grocery">
<h2>Grocery List</h2>
<ul>
  <li>Eggs</li>
  <li>Milk</li>
  <li>Chicken</li>
  <li>Carrots</li>
  <li>Bread</li>
</ul>
</div>
<a href="#top">Back to Top</a>

<div id="State">
<h2>State List</h2>
<ul>
  <li>Ohio
  <ol>
    <li>Cleveland</li>
    <li>Bowling Green</li>
    <li>Columbus</li>
    <li>Toledo</li>
  </ol>
  </li>
  <li>Florida
    <ol>
      <li>Miami</li>
      <li>Tampa</li>
      <li>Orlando</li>
      <li>Jacksonville</li>
    </ol>
  </li>
  <li>Hawaii
    <ol>
      <li>Honolulu</li>
      <li>Hilo</li>
      <li>Pearl City</li>
      <li>Kailua</li>
    </ol>
  </li>
  <li>California
    <ol>
      <li>Los Angeles</li>
      <li>San Diego</li>
      <li>San Francisco</li>
      <li>Sacramento</li>
    </ol>
  </li>
</ul>
</div>
<a href="#top">Back to Top</a>

<div id="Movie">
<h2>Movie List</h2>
<dl>
  <dt>Finding Nemo</dt>
  <dd>
      - Marlin, a clown fish, searches the ocean searching for his son Nemo. <br>
      During Marlin's search for his son, he has to overcome many obstacles <br>
      along the way.
  </dd>
  <dt>Hunger Games</dt>
  <dd>
      - 12 districts fight to the death until one survivor is left.
  </dd>
  <dt>The Platform</dt>
  <dd>
      - Prisoners are on random levels with a roommate. These prisoners eat <br>
      once a day depending on what level they are on. Some do not have any <br>
      food to eat.
  </dd>
  <dt>Fight Club</dt>
  <dd>
      - A man meets a soap salesman and ends up becoming best friends. These 2 <br>
      friends end up creating an undergorund group known as the "Fight Club".
  </dd>
</dl>
</div>
<a href="#top">Back to Top</a>

<div id="Breed">
<table align="center">
  <tr>
    <th colspan="5">Breed Stats</th>
  </tr>
  <tr>
    <th></th>
    <th>Retrievers</th>
    <th>Bulldogs</th>
    <th>German Shorthaired</th>
    <th>Boxers</th>
  </tr>
  <tr>
    <th>Average Height</th>
    <td>20-24 in</td>
    <td>14-18 in</td>
    <td>20-24 in</td>
    <td>14-18 in</td>
  </tr>
  <tr>
    <th>Average Weight</th>
    <td>45-60 lbs</td>
    <td>50-60 lbs</td>
    <td>40-60 lbs</td>
    <td>45-60 lbs</td>
  </tr>
  <tr>
    <th>Example Breed</th>
    <td>Golden Retriever</td>
    <td>English Bulldog</td>
    <td>German Shorthaired Pointer</td>
    <td>American Boxer</td>
  </tr>
</table>
</div>
<a href="#top">Back to Top</a>

<br>

<div id="StateInfo">
<table align="center" id="table2State">
    <tr>
      <th colspan="6">State Info</th>
    </tr>
    <tr>
        <th>State Name</th>
        <td>Ohio</td>
        <td>New York</td>
        <td>Florida</td>
        <td>California</td>
        <td>Hawaii</td>
    </tr>
    <tr>
        <th>State Bird</th>
        <td>Cardinal</td>
        <td>Eastern bluebird</td>
        <td>Northern mockingbird</td>
        <td>California quail</td>
        <td>Nene</td>
    </tr>
    <tr>
        <th>State Flower</th>
        <td>Carnation</td>
        <td>Rose</td>
        <td>Orange blossom</td>
        <td>California poppy</td>
        <td>Yellow hibiscus</td>
    </tr>
    <tr>
        <th>State Tree</th>
        <td>Ohio Buckeye</td>
        <td>Sugar Maple</td>
        <td>Sabal Palm</td>
        <td>Coast redwood</td>
        <td>Candlenut</td>
    </tr>
</table>
</div>
<a href="#top">Back to Top</a>

<br>

<div id="Meals">
<table align="center" >
  <tr>
    <th colspan="5">Meals</th>
  </tr>

  <tr  id="optionalMenu">
    <th colspan="2"></th>
    <th><b>Breakfast<b></th>
    <th><b>Lunch</b></th>
    <th><b>Dinner</b></th>
  </tr>
  <tr>
    <th rowspan="4" style="background-color: #BAECBD; color: chocolate">Foods</th>
    <th>Bread</th>
    <td>Roll</td>
    <td>Sliced Bread</td>
    <td style="background-color: hsla(55,75%,50%,0.5);color: hsl(0, 50%, 35%)">Garlic Bread</td>
  </tr>
  <tr>
    <th>Main Course</th>
    <td>Eggs</td>
    <td>Ham Sandwich</td>
    <td style="background-color: hsla(55,75%,50%,0.5);color: hsl(0, 50%, 35%)">Spaghetti</td>
  </tr>
  <tr>
    <th>Vegetable</th>
    <td>Tomato</td>
    <td>Salad</td>
    <td style="background-color: hsla(55,75%,50%,0.5);color: hsl(0, 50%, 35%)">Brussel Sprouts</td>
  </tr>
  <tr>
    <th>Dessert</th>
    <td>Banana Bread</td>
    <td>Cookies</td>
    <td style="background-color: hsla(55,75%,50%,0.5);color: hsl(0, 50%, 35%)">Ice Cream</td>
  </tr>
</table>
</div>
<a href="#top">Back to Top</a>
</body>

</html>
